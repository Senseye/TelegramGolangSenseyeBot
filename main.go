package main

import (
	"fmt"
	telegram "github.com/go-telegram-bot-api/telegram-bot-api"
	"log"
	"net/http"
	"os"
)

var (
	adviceIndex = 0
	advices     = []string{
		"Find youself",
		"All fine",
		"Life is good",
	}
)

func main() {
	token := os.Getenv("TELEGRAM_SENSEYEBOT_TOKEN")
	if token == "" {
		log.Fatal("Empty Telegram token!")
	}

	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}

	bot, err := telegram.NewBotAPI(token)

	if err != nil {
		log.Panic(err)
	}

	u := telegram.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	go func() {
		for {
			for update := range updates {
				if update.Message == nil {
					continue
				}

				message := telegram.NewMessage(update.Message.Chat.ID, findAnswer(update.Message.Text))
				message.ReplyToMessageID = update.Message.MessageID

				bot.Send(message)
			}
		}
	}()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "@SenseyeBot")
	})
	http.ListenAndServe(":"+port, nil)
}

func findAnswer(text string) string {
	switch text {
	case "/start":
		return "Great, you want know truth!"
	case "/advice":
		advice := advices[adviceIndex%len(advices)]
		adviceIndex++
		return advice
	case "/end":
		return "See you later!"
	}

	return "What are you question?"
}
